# Introduction
About the course and schedules:

* Workshop days:
    * Day 1: Container technologies, Docker, Docker-Compose
        * 9:00 we start with introduction
        * 10:30 - 10:45 coffee break
        * 12:00 - 13:00 lunch break
        * 14:30 - 14:45 coffee break
        * 16:15 closing of the day Q&A 
    * Day 2: Cluster technologies with Docker, Docker-Swarm and Kubernetes
        * 9:00 we start with Q&A-s from previous day. Prepare with questions or be asked to answer one of my questions (called out by name based on participant list.)
        * 10:30 - 10:45 coffee break
        * 12:00 - 13:00 lunch break
        * 14:30 - 14:45 coffee break
        * 16:15 closing of the day Q&A 

* Recording: of the workshop days will be made available online for limited time. It takes some time to process the recordings but you will get it within a day or so.

* Demos and Challenges: will be presented in a copy paste fashion. Time will be given to complete them. If you need to drop off you can complete the demos and challenges based on the recordings.

* Homework: will be announced at the end of the second workshop day. Homework is optional but  highly recommended.

* At the end of the week we will release an online knowledge check quiz.