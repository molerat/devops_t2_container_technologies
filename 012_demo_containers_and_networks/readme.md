## Now try this!
[Docker Playground ](https://labs.play-with-docker.com/)

## Let's create an nginx container
```bash
docker run -d --name mynginx nginx
```
Now if you check the container with inspece
```bash
docker container inspect mynginx
```

You see that it is part of the bridge netwokr:
```json
"Networks":
"bridge": {
    "IPAMConfig": null,
    "Links": null,
    "Aliases": null,
    "NetworkID": "f8592ab8a75ca29540664be079709e61e7e9f89557914135348737b335c8e598",
    "EndpointID": "2245fff41754bf4f2eb1237a06738a75560d7987f30d1207f1731257893639c3",
    "Gateway": "172.17.0.1",
    "IPAddress": "172.17.0.4",
    "IPPrefixLen": 16,
    "IPv6Gateway": "",
    "GlobalIPv6Address": "",
    "GlobalIPv6PrefixLen": 0,
    "MacAddress": "02:42:ac:11:00:04",
    "DriverOpts": null
}
```

Important bits are:
```
"Gateway": "172.17.0.1",
"IPAddress": "172.17.0.4"
```

Every container recives an IP address from a virtual private network.
Each container that is part of the same network can communicate with other containers. You don't even need to expose ports.

Ohh... so there is a default network. Let's check all the networks:
```bash
docker network ls
```
Here you go...
```bash
NETWORK ID          NAME                DRIVER              SCOPE
f8592ab8a75c        bridge              bridge              local
bac65896c6cd        host                host                local
85e237b501c5        none                null                local
```

You can and should create your own networks:
```bash
docker network create app-a-net
docker network create app-b-net
docker network ls
```
Now let's see what we have done...
```bash
NETWORK ID          NAME                DRIVER              SCOPE
bae8044e4361        app-a-net           bridge              local
21baa29e8dbf        app-b-net           bridge              local
f8592ab8a75c        bridge              bridge              local
bac65896c6cd        host                host                local
85e237b501c5        none                null                local
```
Now let's attach our mynginx container to app-a-net network
```bash
docker network connect app-a-net mynginx
```
Then inspect the container's networks portion again:
```json
"Networks": {
    "app-a-net": {
        "IPAMConfig": {},
        "Links": null,
        "Aliases": [
            "8a71a6359760"
        ],
        "NetworkID": "bae8044e436157ad17ff1dcb5d842d41f10d0a8366ee8d65c11920fba78c8ae2",
        "EndpointID": "af0cc8c2e07ede0218dcab6cb96c36094e805a664279df86063a728a94754e47",
        "Gateway": "172.19.0.1",
        "IPAddress": "172.19.0.2",
        "IPPrefixLen": 16,
        "IPv6Gateway": "",
        "GlobalIPv6Address": "",
        "GlobalIPv6PrefixLen": 0,
        "MacAddress": "02:42:ac:13:00:02",
        "DriverOpts": {}
    },
    "bridge": {
        "IPAMConfig": null,
        "Links": null,
        "Aliases": null,
        "NetworkID": "f8592ab8a75ca29540664be079709e61e7e9f89557914135348737b335c8e598",
        "EndpointID": "2245fff41754bf4f2eb1237a06738a75560d7987f30d1207f1731257893639c3",
        "Gateway": "172.17.0.1",
        "IPAddress": "172.17.0.4",
        "IPPrefixLen": 16,
        "IPv6Gateway": "",
        "GlobalIPv6Address": "",
        "GlobalIPv6PrefixLen": 0,
        "MacAddress": "02:42:ac:11:00:04",
        "DriverOpts": null
    }
```
