## Your challnege if you accept it!
Deploy Apache HTTP web server as a container based on the official docker image from dockerhub. 
1. There are a lot of 3rd party images, but which is the official one. Use google to find the right one.
1. Follow the same procedure we did with nginx but this time use te name of the official image.
1. Make sure to run the docker command with the `-d` option to keep it running in the background
1. Make sure to open the `port 80` from the host operating system by using the option `-p 80:80` 
1. Check the logs of the container. Try to browse a non existing page and then check the logs again. Can you follow the logs in `tail -f` fashion?
1. Try to clean  up after yourself (kill and remove the container)


## Approximate time to complete the challenge
You will need 5-10 min of uninterrupted time to complete this challenge.