## Get a Docker Compose Sample
You can get awesome samples for compose from [Awesome Compose git repo](https://github.com/docker/awesome-compose).
```bash
git clone https://github.com/docker/awesome-compose
cd nginx-golang
tree
.
├── README.md
├── backend
│   ├── Dockerfile
│   └── main.go
├── docker-compose.yml
└── frontend
    ├── Dockerfile
    └── nginx.conf
```

## Deploy locally
```bash
docker-compose up -d
```
## Deploy to remote host
You need multiple hosts to run this successfully. So tap that + Add Host button in the sandbox environment.
![](img/multiple_nodes.png)

Now have node1 selected and type:
```bash
scp -r nginx-golang 192.168.0.13:/tmp
ssh 192.168.0.13
cd /tmp/nginx-golang
docker-compose up -d
exit
```

## Using DOCKER_HOST environment variable to set up the target engine
While you are on one host, with the source code of the docker compose enabled project, you can depoly it to another by seeting an environment variable.

```bash
DOCKER_HOST="ssh://192.168.0.13" docker-compose up -d
```
```bash
[node1] (local) root@192.168.0.13 ~
$ docker ps
CONTAINER ID        IMAGE                   COMMAND                  CREATED             STATUS              PORTS                NAMES
e2041a52a9db        nginx-golang_frontend   "/docker-entrypoint.…"   5 seconds ago       Up 3 seconds        0.0.0.0:80->80/tcp   nginx-golang_frontend_1
e3577c7a0f4c        nginx-golang_backend    "/usr/local/bin/back…"   6 seconds ago       Up 4 seconds                             nginx-golang_backend_1
```

## Using docker contexts 
Docker Contexts are an efficient way to automatically switch between different deployment targets. 

```bash
docker context ls
NAME   DESCRIPTION   DOCKER ENDPOINT   KUBERNETES ENDPOINT   ORCHESTRATOR
…
```

Create and use context to target remote host

```bash
docker context create remote --description "Remote host" --docker "host=ssh://192.168.0.13"
```

```bash
docker context ls
NAME                DESCRIPTION                               DOCKER ENDPOINT               KUBERNETES ENDPOINT   ORCHESTRATOR
default *           Current DOCKER_HOST based configuration   unix:///var/run/docker.sock                         swarm
remote              Remote host                               ssh://192.168.0.13
```

```bash
docker context use remote
remote
Current context is now "remote"
```

```bash
docker-compose up -d
```

```bash
docker context use default
```
